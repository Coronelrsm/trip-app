package cat.itb.theworld.adapter;

import android.renderscript.Element;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.OnClick;
import cat.itb.theworld.R;
import cat.itb.theworld.room.Excursion;

public class ExcursionAdapter extends RecyclerView.Adapter<ExcursionAdapter.MyViewHolder> {


    //LiveData<List<Excursion>>
    private LiveData<List<Excursion>> excursions;
    private List<Excursion> excursionDataSet;
    OnElementClickListener onElementClickListener;



    public void setElementsExcursions(List<Excursion> excursionDataSet) {
        this.excursionDataSet = excursionDataSet;
    }

    public void setExcursions(LiveData<List<Excursion>> excursions) {
        this.excursions = excursions;
    }





    //Setter de la interficie

    public void setOnElementClickListener(OnElementClickListener onElementClickListener) {
        this.onElementClickListener = onElementClickListener;
    }





    //constructors

    public ExcursionAdapter(List<Excursion> excursionDataSet) {
        this.excursionDataSet = excursionDataSet;
    }

    public ExcursionAdapter() {
    }






    // Return the size of your dataset
    @Override
    public int getItemCount() {
        return excursionDataSet.size();
    }


    @Override
    public ExcursionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        //TextView v = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false); //Hem de posar aquest?????
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - replace the contents of the view with data.
        holder.titol.setText(excursionDataSet.get(position).getNomExcursio());
        holder.lloc.setText(excursionDataSet.get(position).getLloc());
        holder.preu.setText((int) excursionDataSet.get(position).getPreu());
        holder.dificultat.setText(excursionDataSet.get(position).getDificultat());

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titol;
        public TextView lloc;
        public TextView valoracio;
        public TextView preu;
        public TextView dificultat;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            titol = itemView.findViewById(R.id.titol);
            lloc = itemView.findViewById(R.id.lloc);
            valoracio = itemView.findViewById(R.id.valoracio);
            preu = itemView.findViewById(R.id.preu);
            dificultat = itemView.findViewById(R.id.dificultat);

        }

        @OnClick(R.id.titol)
        public void onItemClicked(){
            Excursion excursion = excursionDataSet.get(getAdapterPosition());
            if(onElementClickListener!=null)
                onElementClickListener.onElementClicked(excursion);
        }


    }

    public interface OnElementClickListener {
        void onElementClicked(Excursion excursion);
    }


}



