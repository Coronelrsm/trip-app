package cat.itb.theworld;

import android.os.Bundle;
import android.renderscript.Element;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.theworld.adapter.ExcursionAdapter;
import cat.itb.theworld.room.Excursion;


public class Excursio extends Fragment {

    @BindView(R.id.titol)
    EditText titol;
    @BindView(R.id.valoracio)
    EditText valoracio;
    @BindView(R.id.dificultat)
    EditText dificultat;
    @BindView(R.id.imatgeExcursio)
    ImageView imatgeExcursio;
    @BindView(R.id.descripcio)
    EditText descripcio;
    @BindView(R.id.recomanacions)
    EditText recomanacions;
    @BindView(R.id.preu)
    EditText preu;
    @BindView(R.id.guardar)
    Button guardar;
    @BindView(R.id.reservar)
    Button reservar;
    @BindView(R.id.lloc)
    EditText lloc;
    public ExcursioViewModel mViewModel;

    public static Excursio newInstance() {
        return new Excursio();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.excursio_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ExcursioViewModel.class);
        // TODO: Use the ViewModel
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


    }

    @OnClick({R.id.guardar})
    public void onViewClicked() {
        String nomExcursio, caracteristiques, altres, ubicacio;
        double cost;
        int grauDificutat;

        nomExcursio = titol.getText().toString().trim();
        ubicacio = lloc.getText().toString().trim();
        grauDificutat = Integer.parseInt(dificultat.getText().toString().trim());
        caracteristiques = descripcio.getText().toString().trim();
        altres = recomanacions.getText().toString().trim();
        cost = Double.parseDouble(preu.getText().toString().trim());
        Excursion trip = new Excursion(nomExcursio, ubicacio, grauDificutat, caracteristiques, altres, cost);

        //constructor modificat perquè no iclogui ID
        mViewModel.guardarExcursio(trip);


    }

}
