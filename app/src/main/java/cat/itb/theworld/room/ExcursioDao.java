package cat.itb.theworld.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao

public interface ExcursioDao {
    @Query("SELECT * FROM excursió")
    LiveData<List<Excursion>> getAll();

    @Insert
    void insertAll(Excursion... excursions); //Classe Excursion, no fragment Excursio!

    @Insert
    void insert(Excursion excursion); //Classe Excursion, no fragment Excursio!
}
