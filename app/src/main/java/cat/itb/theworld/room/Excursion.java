package cat.itb.theworld.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "Excursió")

public class Excursion {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public String nomExcursio;

    @ColumnInfo(name = "Ubicació")
    public String lloc;

    public int dificultat;

    public String descripcio;

    @ColumnInfo(name = "Recomanacions")
    public String altres;

    public double preu;

    public Excursion(String nomExcursio, String lloc, int dificultat, String descripcio, String altres, double preu) {
        this.nomExcursio = nomExcursio;
        this.lloc = lloc;
        this.dificultat = dificultat;
        this.descripcio = descripcio;
        this.altres = altres;
        this.preu = preu;
    }


    public String getNomExcursio() {
        return nomExcursio;
    }

    public String getLloc() {
        return lloc;
    }

    public int getDificultat() {
        return dificultat;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public String getAltres() {
        return altres;
    }

    public double getPreu() {
        return preu;
    }

    public int getId() {
        return id;
    }
}
