package cat.itb.theworld.room;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ExcursioRepository {
    public static ExcursioDao laMevaExcursioDao;


    public ExcursioRepository(Application application) {
        AppDatabase db=AppDatabase.getInstance(application);
        laMevaExcursioDao = db.excursioDao();}

        public void insert(Excursion excursion) {new InsertAsyncTask (laMevaExcursioDao).execute(excursion);}

    public LiveData<List<Excursion>> getExcursions() {
        return laMevaExcursioDao.getAll();
    }


    public static class InsertAsyncTask extends AsyncTask<Excursion, Void, Void> {
        private ExcursioDao mAsyncTaskDao;

        InsertAsyncTask(ExcursioDao laMevaExcursioDao) { mAsyncTaskDao = laMevaExcursioDao;
        }

        @Override
        protected Void doInBackground(final Excursion ... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}



