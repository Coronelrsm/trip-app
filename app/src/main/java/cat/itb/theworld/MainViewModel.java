package cat.itb.theworld;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.theworld.room.ExcursioRepository;
import cat.itb.theworld.room.Excursion;

public class MainViewModel extends ViewModel {
    private ExcursioRepository excursioRepository;
    private LiveData<List<Excursion>> llistaExcursionsLiveData;

    public MainViewModel(@NonNull Application application) {
        excursioRepository = new ExcursioRepository(application);
        llistaExcursionsLiveData = excursioRepository.getExcursions();
    }

    LiveData<List<Excursion>> getExcursions() {
        return llistaExcursionsLiveData;
    }

    public void guardarExcursio(Excursion trip) {

        excursioRepository.insert(trip);

    }
    // TODO: Implement the ViewModel
}
