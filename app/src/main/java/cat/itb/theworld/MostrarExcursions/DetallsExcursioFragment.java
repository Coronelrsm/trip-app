package cat.itb.theworld.MostrarExcursions;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.theworld.R;

public class DetallsExcursioFragment extends Fragment {

    private DetallsExcursioViewModel mViewModel;

    public static DetallsExcursioFragment newInstance() {
        return new DetallsExcursioFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detalls_excursio_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DetallsExcursioViewModel.class);
        // TODO: Use the ViewModel
    }

}
