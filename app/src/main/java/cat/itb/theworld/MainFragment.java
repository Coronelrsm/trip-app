package cat.itb.theworld;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cat.itb.theworld.adapter.ExcursionAdapter;
import cat.itb.theworld.room.Excursion;


public class MainFragment extends Fragment {
    private RecyclerView recyclerView;
    private ExcursionAdapter adapterExcursio;
    private RecyclerView.LayoutManager layoutManager;
    private MainViewModel mViewModel;
    View myView;



    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity());
        adapterExcursio =new ExcursionAdapter();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterExcursio);

        LiveData<List<Excursion>> excursions = mViewModel.getExcursions(); //crida a getAll()
        excursions.observe(this,this::onExcursionChanged);


        adapterExcursio.setOnElementClickListener(this::viewElementDetail);


    }

    private void onExcursionChanged(List<Excursion> excursions) {
            adapterExcursio.setElementsExcursions(excursions);

    }


    //la meva navegacio funciona per id, que és un enter
    private void viewElementDetail(Excursion excursion) {

        MainFragmentDirections.NavigateToExcursionDetails action = MainFragmentDirections.navigateToExcursionDetails(excursion.getId());
        Navigation.findNavController(myView).navigate(action);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // use if changes in content do not change the size of the RecyclerView
        recyclerView = view.findViewById(R.id.recyclerview);
        //recyclerView.setHasFixedSize(true);
        myView = view;
        layoutManager = new LinearLayoutManager(getContext());
        //recyclerView.setLayoutManager(layoutManager);

    }

}




