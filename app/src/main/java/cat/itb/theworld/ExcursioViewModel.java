package cat.itb.theworld;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


import java.util.List;

import cat.itb.theworld.room.ExcursioRepository;
import cat.itb.theworld.room.Excursion;

public class ExcursioViewModel extends AndroidViewModel {
    private ExcursioRepository excursioRepository;
    private LiveData<List<Excursion>> llistaExcursionsLiveData;

    public ExcursioViewModel(@NonNull Application application) {
        super(application);
        excursioRepository = new ExcursioRepository(application);
        llistaExcursionsLiveData = excursioRepository.getExcursions();
    }

    LiveData<List<Excursion>> getExcursions() {
        return llistaExcursionsLiveData;
    }

    public void guardarExcursio(Excursion trip) {

        excursioRepository.insert(trip);

    }

    //mètode per a cridar el dao, li passem la excursió

}
