package cat.itb.theworld;

import android.content.Context;

import androidx.room.Database;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import cat.itb.theworld.room.AppDatabase;
import cat.itb.theworld.room.ExcursioDao;
import cat.itb.theworld.room.Excursion;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        //assertEquals("cat.itb.theworld", appContext.getPackageName());
        AppDatabase db = AppDatabase.getInstance(appContext);
        Excursion excursion = new Excursion("Balconisme", "Montserrat", 2, "Caminada que ...", "sec", 0);
        db.excursioDao().insertAll();
        List<Excursion> excursions = db.excursioDao().getAll();

        assertEquals(excursion.getNomExcursio(), excursions.get(excursions.size()-1).getNomExcursio());





    }
}
